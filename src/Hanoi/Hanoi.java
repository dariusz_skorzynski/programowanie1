package Hanoi;

import java.util.Stack;

public class Hanoi {

    private Stack<Integer> col1 = new Stack();
    private Stack<Integer> col2 = new Stack();
    private Stack<Integer> col3 = new Stack();


    public void addDisks() {
        col1.push(3);
        col1.push(2);
        col1.push(1);

    }
    public void test(){


        int zmienna = col1.pop();
        col2.push(zmienna);
        print();
    }

    public void print() {
        System.out.println("Col1 " + col1);
        System.out.println("Col2 " + col2);
        System.out.println("Col3 " + col3);
    }

    public boolean checkCanMove(int zmienna, int to) {
        switch (to) {
            case 1: {
                if (zmienna < col1.peek()|| col1.isEmpty())
                    return true;
                else return false;
//                break;
            }
            case 2: {
                if (zmienna < col2.peek()|| col2.isEmpty())
                    return true;
                else return false;
            }
            case 3: {
                if (zmienna < col3.peek()|| col3.isEmpty())
                    return true;
                else return false;
            }
        }
        return true;
    }

    public void moveDisk(int from, int to) {
        int zmienna=0;
        switch (from) {
            case 1: {zmienna =col1.peek();
                if (checkCanMove(zmienna, to)) {
                    zmienna = col1.pop();
                }
                break;
            }
            case 2: {zmienna=col2.peek();
//                if (checkCanMove(zmienna, to)) {
//                    zmienna = col2.pop();
//                }
                break;
            }
            case 3: {zmienna=col3.peek();
//                if (checkCanMove(zmienna, to)) {
//                    zmienna = col3.pop();
//                }
 break;
            }
        }
        switch (to) {
            case 1: {
                col1.push(zmienna);
                break;
            }
            case 2: {
                col2.push(zmienna);
                break;
            }
            case 3: {
                col3.push(zmienna);
                break;
            }
        }
    }

    public void setCol1(Stack<Integer> col1) {
        this.col1 = col1;
    }

    public void setCol2(Stack<Integer> col2) {
        this.col2 = col2;
    }

    public void setCol3(Stack<Integer> col3) {
        this.col3 = col3;
    }

    public Stack<Integer> getCol1() {
        return col1;
    }

    public Stack<Integer> getCol2() {
        return col2;
    }

    public Stack<Integer> getCol3() {
        return col3;
    }
}



