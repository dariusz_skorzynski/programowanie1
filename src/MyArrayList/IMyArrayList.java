package MyArrayList;

public interface IMyArrayList<T> {

    void add(T data);
    void add(int index, T data);
    void addSort(T data);
    T get(int index);

    String toString();
void print();
}
