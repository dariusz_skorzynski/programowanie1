package MyLinkedList;

public interface IMyLinkedList<T> {
    void add(T data);
    void add(int index, T data);
    T get(int index);
    String toString();

}
