package MyQueue;
/*
Posługując się ArrayList'ą (jako polem klasy) zaimplementuj klasę która zachowuje się jak kolejka i która posiada metody:
    Stwórz strukturę FIFO która posiada metody:
        - push_back - dodawanie wartości (na koniec)
        - pop_front - wyciąganie wartości (wartość jest zwrócona) (z początku)
        - peek - zwraca następną wartość ale jej nie usuwa (podgląda)
        - size - zwraca rozmiar struktury
        - push_front - dodanie na koniec
        - pop_back - wyciągnięcie z konca kolejki
 */

import java.util.ArrayList;

public class Queue<T> {

    private T object;
    private ArrayList<T> queue = new ArrayList();

    public void push_back(T element) {
        queue.add(queue.size(), element);
    }

    public T pop_front() {
        T temp = queue.get(0);
        queue.remove(0);
        return temp;
    }

    public void pushfront(T element) {
        queue.add(0, element);
    }

    public T peek_front() {
        T temp = queue.get(0);
        return temp;
    }

    public int size() {
        return queue.size();
    }

    public T back() {
        T temp = queue.get(queue.size() - 1);
        queue.remove(queue.size() - 1);
        return temp;

//         return queue.remove((queue.size()-1));
        // usuwając pobieramy element i mozna to wykorzystać.

    }

}
