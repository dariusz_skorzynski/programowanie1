package Sortowanie;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random r = new Random();
        int rozmiar = 20;
        int[] A = new int[rozmiar];
        for (int i = 0; i < rozmiar; i++) {
            A[i] = r.nextInt(100);
        }
        System.out.print(Arrays.toString(A));
        System.out.println("\n");

//        Sortowanie_przez_wstawianie sortowanie_przez_wstawianie = new Sortowanie_przez_wstawianie();
//        sortowanie_przez_wstawianie.insertsort(A);

        Sortowanie_Babelkowe sortowanie_babelkowe = new Sortowanie_Babelkowe();
        sortowanie_babelkowe.sort(A);
//        System.out.println(Arrays.toString(A));
    }
}
