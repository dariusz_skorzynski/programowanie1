package Sortowanie;

import java.util.Arrays;

/*
for j in range(0,n):
for i in range(0,n-1):
if(A[i] > A[i+1]):
A[i],A[i+1] = A[i+1],A[i]
 */
public class Sortowanie_Babelkowe {

    public void sort(int[] A){
        for (int i = 0; i <A.length ; i++) {
            for (int j = 0; j <A.length-1 ; j++) {
                if(A[j]>A[j+1]){
                    int tmp = A[j];
                    A[j] = A[j+1];
                    A[j+1]=tmp;
                    System.out.println(Arrays.toString(A));
                }
            }
        }
    }
//    public void print()
}
