package Sortowanie;

/*
0. Insert_sort(A, n)
1. for i=1 to n :
2. klucz = A[i]
3 # Wstaw A[i] w posortowany ciąg A[0 ... i-1]
4. j = i - 1
5. while j>=0 and A[j]>klucz:
6. A[j + 1] = A[j]
7. j = j - 1
8.
A[j + 1] = klucz
 */
public class Sortowanie_przez_wstawianie {


    public void insertsort(int[] A) {

        for (int i = 1; i < A.length; i++) {
            int temp = A[i];
            int j = i-1;
            while (j >= 0 && A[j] >=temp) {
                A[j + 1] = A[j];
                j = j - 1;
            }
                A[j +1] = temp;


        }
    }


}
