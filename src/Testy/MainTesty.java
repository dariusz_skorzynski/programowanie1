package Testy;

import java.util.Arrays;
import java.util.Stack;

public class MainTesty {
    public static void main(String[] args) {


        Stack<Integer> s1 = new Stack<>();
        Stack<Integer> s2 = new Stack<>();
        Stack<Integer> s3 = new Stack<>();
        Stack<Integer> s4 = new Stack<>();

        s1.addAll(Arrays.asList(1, 3, 7, 11, 13, 17));
        s2.addAll(Arrays.asList(1, 1, 2, 3, 5, 8, 13, 21, 34));

        Stack<Integer> s5 = (Stack<Integer>) s1.clone();
        Stack<Integer> s6 = (Stack<Integer>) s2.clone();

        System.out.println(s1);
        System.out.println(s2);

        System.out.println();

        s3 = decreasingMerge(s1, s2, s3);
        System.out.println(s3);

        System.out.println();

        s4 = increasingMerge(s5, s6, s4);
        System.out.println(s4);

    }

    private static Stack<Integer> increasingMerge(Stack<Integer> stack1, Stack<Integer> stack2, Stack<Integer> mergedStack) {
        Stack<Integer> tempStack = new Stack<>();

        while (!stack1.empty() || !stack2.empty()) {

            if (!stack1.empty() && !stack2.empty()) {
                int e1 = stack1.peek();
                int e2 = stack2.peek();

                if (e1 > e2) {
                    tempStack.push(e2);
                    stack2.pop();
                } else {
                    tempStack.push(e1);
                    stack1.pop();
                }
                continue;
            }
            if (stack1.empty() && !stack2.empty()) {
                tempStack.push(stack2.pop());
            }
            if (!stack1.empty() && stack2.empty()) {
                tempStack.push(stack1.pop());
            }
        }
        return tempStack;

    }

    private static Stack<Integer> decreasingMerge(Stack<Integer> stack1, Stack<Integer> stack2, Stack<Integer> mergedStack) {
        Stack<Integer> tempStack = new Stack<>();

        while (!stack1.empty() || !stack2.empty()) {

            if (!stack1.empty() && !stack2.empty()) {
                int e1 = stack1.peek();
                int e2 = stack2.peek();

                if (e1 > e2) {
                    tempStack.add(0, e2);
                    stack2.pop();
                } else {
                    tempStack.add(0, e1);
                    stack1.pop();
                }
                continue;
            }
            if (stack1.empty() && !stack2.empty()) {
                tempStack.add(0, stack2.pop());
            }
            if (!stack1.empty() && stack2.empty()) {
                tempStack.add(0, stack1.pop());
            }
        }
        return tempStack;
    }
}


