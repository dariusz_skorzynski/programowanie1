package Zadanie_55;

import Sortowanie.Main;

import java.util.Scanner;

public class Zadanie55 {
    /*
    Napisz program wyznaczający najmniejszą wspólną wielokrotność (NWW) dla pary liczb całkowitych dodatnich
hint:
Obliczaj wielokrotności jednej liczby i sprawdzaj, czy obliczona wielokrotność jest wielokrotnością drugiej liczby
(czy dzieli się bez reszty przez drugą liczbę)
     */
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int liczba1 = 0;
        int liczba2 = 0;

        System.out.println("Podaj 2 liczby");
        String[] inputArgs = sc.nextLine().split(" ", 2);

        try {
            liczba1 = Integer.parseInt(inputArgs[0]);
            liczba2 = Integer.parseInt(inputArgs[1]);

        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }

        System.out.println("Najmiejszy wspólny dzielnik to: "+nww(liczba1, liczba2));
    }

    private static int nww(int liczba1, int liczba2) {
        int nww = 0;
        for (int i = 1; i <= liczba1 * liczba2; i++) {
            if (i % liczba1 == 0 && i % liczba2 == 0) {
                nww = i;
                break;
            }
        }
            return nww;
        }
    }


