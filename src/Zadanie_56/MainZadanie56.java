package Zadanie_56;

import java.util.Scanner;

public class MainZadanie56 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int liczba1 = 0;
        int liczba2 = 0;

        System.out.println("Podaj 2 liczby");
        String[] inputArgs = sc.nextLine().split(" ", 2);

        try {
            liczba1 = Integer.parseInt(inputArgs[0]);
            liczba2 = Integer.parseInt(inputArgs[1]);

        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }
        System.out.println(nwd(liczba1,liczba2));
    }

    public static int nwd(int liczba1, int liczba2) {
        int d1 = Math.min(liczba1,liczba2);
        int d2 = Math.max(liczba1,liczba2);
        while (d1!=d2){
            int td1 =Math.min(d1,d2);
            int td2 =Math.max(d1,d2)-Math.min(d1,d2);
            d1=td1;
            d2=td2;
        }return d1;

    }
}
