package Zadanie_57;

import java.util.Scanner;

/*
Napisz program, który nie wykonując mnożenia, obliczy kwadrat liczby naturalnej
hint:
Wykorzystaj fakt, e suma kolejnych liczb nieparzystych jest kwadratem liczby naturalnej:
1 = 1^2, 1+3 = 4 = 2^2, 1+3+5 = 9 = 3^2, 1+3+5+7 = 16 = 4^2
 */
public class MainZadanie57 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną:");
        int liczba = sc.nextInt();
        int suma=0;
        for (int i = 1; i <=liczba*2 ; i++) {
            if(i%2 ==1){
            suma =suma +i;
            }
        }
        System.out.println(suma);
    }
}
