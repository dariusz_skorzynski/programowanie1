package Zadanie_71;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Magazyn {
    private Produkt[] tablica = new Produkt[6];

    @Override
    public String toString() {
        return "Magazyn{" +
                "tablica=" + Arrays.toString(tablica) +
                '}';
    }

    public Produkt[] getTablica() {
        return tablica;
    }

    public void addProdukt(String nazwa, int cena, int ilosc) {

        int counter = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] != null) {
                counter++;
            }}
            if(counter<tablica.length){
                tablica[counter] = new Produkt(nazwa, cena, ilosc);

        }else System.out.println("Magazyn pełny");
    }
public void printTablica (Produkt [] tab){
    for (int i = 0; i <tab.length ; i++) {
        if (tab[i]!=null){
            Produkt p = tab[i];
            System.out.printf("Nazwa %8s   Cena %5s   Ilość %5s \n", p.getNazwa(), p.getCena(), p.getIlosc());

        }

    }
}
    public Produkt[] sortCena(){
        Produkt[] tablicatemp = tablica;
        for (int i = 0; i <tablica.length ; i++) {
            for (int j = 0; j <tablica.length-1 ; j++) {
                if(tablica[j].getCena()>tablica[j+1].getCena()){
                    Produkt tmp = tablica[j];
                    tablica[j] = tablica[j+1];
                    tablica[j+1]=tmp;

                }
            }
        }return tablicatemp;
    } public Produkt[] sortIlosc(){
        Produkt[] tablicatemp = tablica;
        for (int i = 0; i <tablica.length ; i++) {
            for (int j = 0; j <tablica.length-1 ; j++) {
                if(tablica[j].getIlosc()>tablica[j+1].getIlosc()){
                    Produkt tmp = tablica[j];
                    tablica[j] = tablica[j+1];
                    tablica[j+1]=tmp;

                }
            }
        }return tablicatemp;
    }public Produkt[] sortNazwa(){
        Produkt[] tablicatemp = tablica;
        for (int i = 0; i <tablica.length ; i++) {
            for (int j = 0; j <tablica.length-1 ; j++) {
                if(tablica[j].getNazwa().compareTo(tablica[j+1].getNazwa()) >=0){
                    Produkt tmp = tablica[j];
                    tablica[j] = tablica[j+1];
                    tablica[j+1]=tmp;

                }
            }
        }return tablicatemp;
    }


}


