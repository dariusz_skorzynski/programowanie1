package Zadanie_71;

public class Produkt {
    private String nazwa;
    private int cena;
    private int ilosc;

    public String getNazwa() {
        return nazwa;
    }

    public int getCena() {
        return cena;
    }

    public int getIlosc() {
        return ilosc;
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "nazwa='" + nazwa + '\'' +
                ", cena=" + cena +
                ", ilosc=" + ilosc +
                '}' + "\n";
    }

    public Produkt(String nazwa, int cena, int ilosc) {

        this.nazwa = nazwa;
        this.cena = cena;
        this.ilosc = ilosc;
    }
}
