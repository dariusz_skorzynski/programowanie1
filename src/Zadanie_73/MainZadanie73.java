package Zadanie_73;

import java.util.Arrays;
import java.util.Stack;

public class MainZadanie73 {
    public static void main(String[] args) {

        Stack<Integer> stack1 = new Stack();
        Stack<Integer> stack2 = new Stack();
        Stack<Integer> stack3 = new Stack();
        Stack<Integer> stack4 = new Stack();


        stack1.addAll(Arrays.asList(1, 3, 7, 11, 13, 17));
        stack2.addAll(Arrays.asList(1, 1, 2, 3, 5, 8, 13, 21, 34));
        System.out.println(stack1);
        System.out.println(stack2);


        while (!stack1.isEmpty() || !stack2.empty()) {

            if (!stack1.empty() && !stack2.empty()) {
                int e1 = stack1.peek();
                int e2 = stack2.peek();

                if (e1 < e2) {
                    stack3.push(e2);
                    stack2.pop();
                } else {
                    stack3.push(e1);
                    stack1.pop();
                }
                continue;
            }
            if (stack1.empty() && !stack2.empty()) {
                stack3.push(stack2.pop());
            }
            if (!stack1.empty() && stack2.empty()) {
                stack3.push(stack1.pop());
            }
        }
        System.out.println("Posortowane malejąco: "+stack3 + "\n");
        stack1.addAll(Arrays.asList(1, 3, 7, 11, 13, 17));
        stack2.addAll(Arrays.asList(1, 1, 2, 3, 5, 8, 13, 21, 34));
        System.out.println(stack1);
        System.out.println(stack2);

        while (!stack1.isEmpty() || !stack2.empty()) {

            if (!stack1.empty() && !stack2.empty()) {
                int e1 = stack1.peek();
                int e2 = stack2.peek();

                if (e1 < e2) {
                    stack4.add(0, e2);
                    stack2.pop();
                } else {
                    stack4.add(0, e1);
                    stack1.pop();
                }
                continue;
            }
            if (stack1.empty() && !stack2.empty()) {
                stack4.add(0, stack2.pop());
            }
            if (!stack1.empty() && stack2.empty()) {
                stack4.add(0, stack1.pop());
            }
        }
        System.out.println("Posortowane rosnąco: "+stack4);
    }
}