package Zadanie_Names;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Names {

    List<String> listaNazwisk = new ArrayList<>();
    List<String> listaImion = new ArrayList<>();
    Map<String, Integer> forenames = new HashMap<>();
    Map<String, Integer> surnames = new HashMap<>();
    public void createCounters(String ciag) {

        String tablica[] = ciag.toLowerCase().split(" ");
        for (int i = 0; i < tablica.length; i++) {
            if (i % 2 == 0) {
                listaNazwisk.add(tablica[i]);
            } else {
                listaImion.add(tablica[i]);
            }
        }

        for (String line : listaImion) {
            String[] e = line.toLowerCase().split(" ");
            String forename = e[0];

            if (forenames.containsKey(forename)) {
                forenames.put(forename, forenames.get(forename) + 1);
            } else {
                forenames.put(forename, 1);
            }
        }
        for (String line : listaNazwisk) {
            String[] e = line.toLowerCase().split(" ");
            String surname = e[0];

            if (surnames.containsKey(surname)) {
                surnames.put(surname, surnames.get(surname) + 1);
            } else {
                surnames.put(surname, 1);
            }
        }
    }

    public void print() {

        for (Map.Entry<String, Integer> e : forenames.entrySet()) {
            String forename = e.getKey();
            Integer counter = e.getValue();
            System.out.printf("%s%s, %d\n",
                    String.valueOf(forename.charAt(0)).toUpperCase(),
                    forename.substring(1),
                    counter);

        }
        System.out.println();
        for (Map.Entry<String, Integer> e : surnames.entrySet()) {
            String surname = e.getKey();
            Integer counter = e.getValue();
            System.out.printf("%s%s, %d\n",
                    String.valueOf(surname.charAt(0)).toUpperCase(),
                    surname.substring(1),
                    counter);
        }

    }
}
