package Zadanie_PrzydatnośćProduktów;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Magazyn {

//    private int ilosc;
//    private int iloscStarych;
//    private int iloscNowych;

    private Map<String, List<Produkt>> mapaNazwaProdukt = new HashMap<>();
    private PriorityQueue<Produkt> kolejkaDataProdukt = new PriorityQueue<>();




    public void dodaj(String nazwa, Double cena, int ilosc, LocalDateTime dataWaznosci){
        if(mapaNazwaProdukt.containsKey(nazwa)){
            for (Produkt p : mapaNazwaProdukt.get(nazwa)) {
                p.setCena(cena);
            }
            for (int i = 0; i < ilosc; i++) {
                Produkt p = new Produkt (nazwa, cena, dataWaznosci);
                mapaNazwaProdukt.get(nazwa).add(p);
                kolejkaDataProdukt.add(p);
            }
        }
        else {
            mapaNazwaProdukt.put(nazwa, new ArrayList<>());
            for (int i = 0; i < ilosc; i++) {
                Produkt p = new Produkt (nazwa, cena, dataWaznosci);
                mapaNazwaProdukt.get(nazwa).add(p);
                kolejkaDataProdukt.add(p);
            }
        }
    }



    public void usun(String nazwa){
        if (mapaNazwaProdukt.containsKey(nazwa)) {
            mapaNazwaProdukt.remove(nazwa);
        }

        PriorityQueue<Produkt> pq = new PriorityQueue<>(kolejkaDataProdukt);
        Iterator<Produkt> it = pq.iterator();

        while (it.hasNext()){
            Produkt p = it.next();
            if(p.getNazwa().equals(nazwa)){
                kolejkaDataProdukt.remove(p);
            }
        }
    }

    public void sprawdz(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd-HH:mm");
        Map<String, Map<LocalDateTime, Integer>> mapaIlosci = new TreeMap<>();
        for (Map.Entry<String, List<Produkt>> para : mapaNazwaProdukt.entrySet()) {
            mapaIlosci.put(para.getKey(), new TreeMap<>());
            for (Produkt p : para.getValue()) {
                if(!mapaIlosci.get(p.getNazwa()).containsKey(p.getDataWaznosci())){
                    mapaIlosci.get(p.getNazwa()).put(p.getDataWaznosci(), 1);
                }
                else {
                    int ilosc = mapaIlosci.get(p.getNazwa()).get(p.getDataWaznosci());
                    ilosc += 1;
                    mapaIlosci.get(p.getNazwa()).put(p.getDataWaznosci(), ilosc);
                }
            }
        }
        if (!mapaIlosci.isEmpty()) {
            for (Map.Entry<String, Map<LocalDateTime, Integer>> entry: mapaIlosci.entrySet()) {
                for (Map.Entry<LocalDateTime, Integer> mapaDataIlosc : entry.getValue().entrySet()) {
                    Double cena = mapaNazwaProdukt.get(entry.getKey()).get(0).getCena();
                    LocalDateTime data = mapaDataIlosc.getKey();
                    System.out.println(entry.getKey() + " cena: " + cena + ", " + data.format(dtf) + "-> ilość: " + mapaDataIlosc.getValue() + " - " + termin(data));
                }
            }
        } else {
            System.out.println("Magazyn pusty.");
        }
        System.out.println();
        System.out.println();
    }

    public void wywal(){
        for (Produkt p : kolejkaDataProdukt) {
            System.out.println(p.getNazwa() + " " + p.getCena() + " " + p.getDataWaznosci());
        }
    }

    private String termin(LocalDateTime data){
        if(data.isAfter(LocalDateTime.now())) return "zdatne do spożycia";
        else return "przeterminowane";
    }
    public Map<String, List<Produkt>> getMapaNazwaProdukt() {
        return mapaNazwaProdukt;
    }


}
