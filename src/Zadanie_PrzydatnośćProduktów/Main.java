package Zadanie_PrzydatnośćProduktów;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Magazyn m = new Magazyn();

        Scanner skan = new Scanner(System.in);
        String inputLine = "";

        while (!inputLine.equals("quit")){
            System.out.println("Podaj komende: dodaj + nazwa + cena + ilosc + data/ usun + nazwa / sprawdz");
            inputLine = skan.nextLine();
            String[] inputArgs = inputLine.split(" ");
            if(inputArgs[0].equals("dodaj")){
                try {
                    String nazwa = inputArgs[1];
                    Double cena = Double.parseDouble(inputArgs[2]);
                    int ilosc = Integer.parseInt(inputArgs[3]);
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd-HH:mm");
                    LocalDateTime dataWaznosci = LocalDateTime.parse(inputArgs[4], dtf);
                    m.dodaj(nazwa, cena, ilosc, dataWaznosci);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
            else if (inputArgs[0].equals("usun")){
                try {
                    m.usun(inputArgs[1]);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
            else if(inputArgs[0].equals("sprawdz")){
                m.sprawdz();
            }
        }
    }
}
