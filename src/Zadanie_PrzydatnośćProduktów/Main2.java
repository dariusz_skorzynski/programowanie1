package Zadanie_PrzydatnośćProduktów;

import java.time.LocalDateTime;
import java.util.PriorityQueue;
import java.util.Queue;

public class Main2 {
    public static void main(String[] args) {
        Magazyn m = new Magazyn();

        Queue<Produkt> mamySobieKolejke = new PriorityQueue<>();
        mamySobieKolejke.add(new Produkt("nazwa1", 3.5, LocalDateTime.now().plusWeeks(2)));
        mamySobieKolejke.add(new Produkt("nazwa2", 3.5, LocalDateTime.now().plusWeeks(1)));
        mamySobieKolejke.add(new Produkt("nazwa3", 3.5, LocalDateTime.now().plusWeeks(3)));

        System.out.println(mamySobieKolejke);
    }
}
