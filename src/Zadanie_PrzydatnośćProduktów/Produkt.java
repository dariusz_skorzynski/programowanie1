package Zadanie_PrzydatnośćProduktów;

import java.time.LocalDateTime;

public class Produkt implements Comparable {
    private String nazwa;
    private Double cena;
    private LocalDateTime dataWaznosci;


    public Produkt(String nazwa, Double cena, LocalDateTime dataWaznosci) {
        this.nazwa = nazwa;
        this.cena = cena;
        this.dataWaznosci = dataWaznosci;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }

    public String getNazwa() {
        return nazwa;
    }

    public LocalDateTime getDataWaznosci() {
        return dataWaznosci;
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "nazwa='" + nazwa + '\'' +
                ", cena=" + cena +
                ", dataWaznosci=" + dataWaznosci +
                '}' + "\n";
    }

    @Override
    public int compareTo(Object o) {
            return dataWaznosci.compareTo(((Produkt) o).getDataWaznosci());
    }
}
